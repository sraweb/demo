(function(){
'use strict';

console.log('=model-tag=');

const DEBUG = true;

let injected = false;

function injectDependencies() {
  if (injected) {
    console.log('already injected');
    return;
  }

  injectStyles();
  injectModelViewerScript();

  injected = true;
}

function injectModelViewerScript() {
  console.log('inject model-viewer script');
  (function(d, script) {
      script = d.createElement('script');
      script.type = 'module';
      script.async = true;
      script.src = 'https://unpkg.com/@google/model-viewer@1.9.2/dist/model-viewer.min.js';
      d.getElementsByTagName('head')[0].appendChild(script);
  }(document));
}

function injectStyles() {
  const styleString =`
model, model-tag {
  display: block;
  background-color: #ccc;
  outline: 1px solid blue;
  min-width: 100px;
  min-height: 100px;
}
`;
  const style = document.createElement('style');
  style.textContent = styleString;
  document.head.append(style);
}

class Model extends HTMLElement {
  #modelSrc;
  #viewEl;

  constructor() {
    super();
    console.log('constructor');

    injectDependencies();

    this._shadowRoot = this.attachShadow({ 'mode': 'open' });

    const template = document.createElement('template');
    template.innerHTML = `
      <style>
      model-viewer {
        background-color: #ccc;
        width: 100%;
        height: 100%;
      }
      model-viewer#reveal {
        --poster-color: transparent;
      }
      <slot></slot>
      </style>
    `;
    this._shadowRoot.appendChild(template.content.cloneNode(true));

    this.#modelSrc = '';

    // indicate the policy for loading when outside the viewport
    const LOADING_ATTR = 'loading';
    // alternative textual display
    const ALT_ATTR = 'alt';


    let mvEl = document.createElement('model-viewer');
    mvEl.setAttribute('ar', '');
    mvEl.setAttribute('ar-modes', 'scene-viewer quick-look');
    mvEl.setAttribute('shadow-intensity', '1');
    this.#viewEl = mvEl;

    this.#modelSrc = '';
    this.processSources();

    this._shadowRoot.appendChild(mvEl);
  }

  // read-only string returning the URL to the loaded resource
  get currentSrc() {
    return this.#modelSrc;
  }

  hasAudio() {
    return new Promise((resolve, reject) => {
      setTimeout(() => { console.log('hasAudio: false');  resolve(false); }, 500);
    });
  }

  connectedCallback() {
    console.log('model connectedCallback');

    let pS = this.processSources.bind(this);

    const config = { attributes: true, childList: true, subtree: true };
    const callback = function(mutationsList, observer) {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList') {
          console.log('A child node has been added or removed');
          console.log(mutation);
          pS();
        } else if (mutation.type === 'attributes') {
          console.log('The ' + mutation.attributeName + ' attribute was modified');
          console.log(mutation);
          pS();
        }
      }
    };

    const observer = new MutationObserver(callback);
    this._observer = observer;

    observer.observe(this, config);

    this.addEventListener('slotchange', e => {
      console.log('dom children changed!');
    });
  }

  disconnectedCallback() {
    console.log('model connectedCallback');
    this._observer.disconnect();
  }

  static get observedAttributes() {
    return ['autoplay', 'interactive', 'loading'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    console.log(`model attributeChangedCallback: ${name}: '${oldValue}' -> '${newValue}'`);

    // boolean indicating whether the model can be interacted with
    if (name === 'interactive') {
      if (newValue === null) {
        this.#viewEl.removeAttribute('camera-controls');
      } else {
        this.#viewEl.setAttribute('camera-controls', '');
      }
    }

    // boolean whether the model will automatically start playback
    if (name === 'autoplay') {
      if (newValue === null) {
        this.#viewEl.removeAttribute('autoplay');
      } else {
        this.#viewEl.setAttribute('autoplay', '')
      }
    }
  }

  processSources() {
    const GLTF_MIME = 'model/gltf+json';
    const GLTF_BIN_MIME = 'model/gltf-binary';

    let sources = this.querySelectorAll(':scope > source');

    let modelUrl = '';
    for (let i = 0; i < sources.length; i++) {
      let source = sources[i];
      let type = source.getAttribute('type');
      if (type === GLTF_MIME || type === GLTF_BIN_MIME) {
        let src = source.getAttribute('src');
        console.log('found valid source: ' + src);
        modelUrl = src;
        break;
      }
    }
    this.#viewEl.setAttribute('src', modelUrl);
    this.#modelSrc = modelUrl;
  }

}

window.customElements.define('model-tag', Model);

////

})();